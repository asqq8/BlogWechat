/* HiTypecho-微信小程序版Typecho
   使用教程：www.hiai.top
   有任何使用问题请联系作者邮箱 goodsking@163.com
*/
//logs.js
const util = require('../../utils/util.js')
Page({
  data: {
    logs: []
  },
  onLoad: function () {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return util.formatTime(new Date(log))
      })
    })
  }
})
